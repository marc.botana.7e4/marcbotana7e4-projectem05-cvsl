package ConillsVsLlopsSim.Class;

public abstract class Animal{
    int x, y;
    public boolean mort = false;
    public abstract String toString();
    public void mor() { this.mort = true; }
    //Mètode mou per fer "Override"
    public void mou(){}
    public Animal(int x, int y){
        this.x = x;
        this.y = y;
    }
}