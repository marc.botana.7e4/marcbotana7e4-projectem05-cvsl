package ConillsVsLlopsSim.Class;

import java.util.Objects;

public class Bloc{
    private final int terreny; // 0(terra, verd) 1(roca, gris) 2(aigua, blau)
    private Animal animal = null;
    private Pastanaga pastanaga = null;
    public String toString(){
        String grafic;
        switch (terreny){
            case 1: grafic = "\033[0;100m"; break;
            case 2: grafic = "\u001B[44m"; break;
            default: grafic = "\u001B[42m"; // terra per defecte
        }
        // 2 espais per a mantindre l'alineació
        return grafic + Objects.requireNonNullElse(animal, "  ") + "\033[0m"; // reset, torna al color per defecte

    }
    public Bloc(int terreny){
        this.terreny = terreny;
    }

    // Constructor del bloc d'herba amb una pastanaga.
    public Bloc(int terreny, Pastanaga pastanaga){
        this.terreny = terreny;
        this.pastanaga = pastanaga;
    }

    public Bloc (int terreny, Animal animal){
        this.animal = animal;
        this.terreny = terreny; // this(terreny); no era així per a reutilitzar un constructor sobrecarregat? :S
    }

    //Afegir els mètodes getPastanaga
    public boolean esPastanaga() { return pastanaga != null; }
    public Pastanaga getPastanaga() {return this.pastanaga; }

    public boolean esAigua() {
        return terreny != 2;
    }
    public boolean esRoca() {
        return terreny == 1;
    }
    public boolean esConill(){
        return animal instanceof Conill;
    }
    public boolean esLlop(){
        return animal instanceof Llop;
    }
    public Animal getAnimal() { return this.animal; }
    public void delAnimal(){
        this.animal = null;
    }
    public void setAnimal(Animal animal){
        this.animal = animal;
    }

}