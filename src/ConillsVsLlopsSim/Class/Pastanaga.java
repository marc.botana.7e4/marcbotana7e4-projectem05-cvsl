package ConillsVsLlopsSim.Class;


public class Pastanaga {
    //Semblant a la classe animal, pero sense el mètode mou, és el menjar que tindràn els conills

    int x, y;
    public boolean menjat = false;
    public String toString() { return "\uD83E\uDD55"; } // icona de pastanaga

    public void menjar() { this.menjat = true; }
    public Pastanaga(int x, int y){
        this.x = x;
        this.y = y;
    }
}

