package ConillsVsLlopsSim;//versió 0.1
import ConillsVsLlopsSim.Class.*;

import java.util.ArrayList;
        import java.util.Scanner;

public class ConillsVsLlopsSim {
    public static final int CONILLS = 5, LLOPS = 6, COLUMNES = 16, FILES = 16, ROQUES = 10, AIGUA = 15, PASTANAGA = 4;
    public static ArrayList<Animal> animals = new ArrayList<>();
    public static ArrayList<Pastanaga> pastanagues = new ArrayList<>(); //Llista de pastanagues.
    public static Bloc[][] tauler = new Bloc[COLUMNES][FILES];
    private int torns = 0;

    public void inicialitza(){
        for (int i = 0; i < COLUMNES ; i++) {
            for (int j = 0; j < FILES; j++) {
                int terreny = (int) (Math.random() * 100), t = 0;
                int animal = (int) (Math.random() * 100);
                int pastanaga = (int) (Math.random() * 100);
                if (terreny < ROQUES) t = 1;
                else if (terreny < ROQUES + AIGUA) t = 2;

                Animal a;
                if (t == 2 || animal >= LLOPS + CONILLS) {
                    tauler[i][j] = new Bloc(t);
                }
                else{
                    if (animal < LLOPS) {
                        a = new Llop(i, j);
                    } else {
                        a = new Conill(i, j);
                    }
                    animals.add(a);
                    tauler[i][j] = new Bloc(t, a);
                }

                // Afegir pastanagues al tauler.
                Pastanaga p;
                if (t == 2 || pastanaga >= PASTANAGA) {
                    tauler[i][j] = new Bloc(t);
                }
                else{
                    p = new Pastanaga(i, j);
                    pastanagues.add(p);
                    tauler[i][j] = new Bloc(t, p);
                }
            }
        }
    }
    public void mostra(){
        int numConills = 0;
        int numLlops = 0;

        for (int i = 0; i < FILES; i++) {
            for (int j = 0; j < COLUMNES; j++) {
                System.out.print(tauler[i][j]);
            }
            System.out.println();
        }

        //Contador per ha saber el número de conills i llops que hi han en aquell torn.
        for (Animal a : animals) {
            if (a.toString().equals("\uD83D\uDC30"))
                numConills ++;
            else
                numLlops ++;
        }
        System.out.println(" \uD83D\uDC30" + numConills + "/" + "\uD83D\uDC3A" + numLlops + " \uD83D\uDD50" + torns + " \uD83E\uDD55" + pastanagues.size());
    }

    public static void main(String[] args){

        ConillsVsLlopsSim joc = new ConillsVsLlopsSim();
        joc.inicialitza();
        Scanner scanner = new Scanner(System.in);
        while (animals.size() > 0){
            ArrayList<Animal> morts = new ArrayList<>();
            ArrayList<Pastanaga> pMenjat = new ArrayList<>(); //Llista de pastanagues menjades.
            for (Animal animal : animals){
                animal.mou();
            }
            for (Animal animal : animals){
                if(animal.mort) morts.add(animal);
            }
            for (Animal animal : morts){
                animals.remove(animal);
            }
            //Afegir pastanagues a la llista "pMenjat" i esborrar-les.
            for (Pastanaga p : pastanagues){
                if(p.menjat) pMenjat.add(p);
            }
            for (Pastanaga p : pMenjat){
                pastanagues.remove(p);
            }
            joc.torns ++;
            joc.mostra();
            scanner.nextLine();
        }
        joc.mostra();
        System.out.println("Has destruit tota vida a la vista, bon treball! \uD83D\uDE05");
    }
}

